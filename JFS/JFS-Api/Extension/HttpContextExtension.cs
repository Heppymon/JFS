﻿using JFS_Api.Infrastructure.Auth;
using System.Net;
using System.Text.Json;

namespace JFS_Api.Extension
{
    public static class HttpContextExtension
    {
        public static async Task SendResponse(this HttpContext httpContext, string message, string messageId, HttpStatusCode statusCode, Exception e)
        {
            if (httpContext.Response.HasStarted)
                throw e;

            httpContext.Response.Clear();
            httpContext.Response.StatusCode = (int)statusCode;
            httpContext.Response.ContentType = "application/json";
            var problemDetail = new ErrorMessageResponse
            {
                Message = message,
                MessageId = messageId
            };

            await httpContext.Response.WriteAsync(JsonSerializer.Serialize(problemDetail));
        }

    }
}
