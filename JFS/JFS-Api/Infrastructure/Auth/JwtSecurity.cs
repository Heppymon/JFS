﻿using JFS_Api.Exceptinos;
using JFS_Api.Infrastructure.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JFS_Api.Infrastructure.Auth
{
    public class JwtSecurity : IJwtSecurity
    {
        private static readonly Microsoft.IdentityModel.Tokens.SymmetricSecurityKey SymmetricSecurityKey
            = new(Encoding.UTF8.GetBytes(JwtConstants.SymmetricSecurityKeyString));

        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly TokenSettings tokenSettings;

        public JwtSecurity(IHttpContextAccessor httpContextAccessor, IOptions<TokenSettings> tokenSettingsConfig)
        {
            this.httpContextAccessor = httpContextAccessor;
            tokenSettings = tokenSettingsConfig.Value;
        }

        public string JwtGenerate(Guid userId, string email, string policyLevel)
        {
            var notBefore = DateTime.UtcNow;
            var expires = notBefore.AddMinutes(tokenSettings.AccessTokenLifeMinutes);
            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Sub, email),
                new(JwtRegisteredClaimNames.NameId, userId.ToString()),
                //new(JwtConstants.Email, email),
                new(JwtConstants.Policy, policyLevel),
                new(JwtConstants.Role, "none")
            };

            var signingCredentials = new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha512);
            var token = new JwtSecurityToken(JwtConstants.Issuer, JwtConstants.Audience, claims, expires: expires,
                notBefore: notBefore,
                signingCredentials: signingCredentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private IEnumerable<Claim> GetClaims()
        {
            if (httpContextAccessor.HttpContext?.User?.Identity?.IsAuthenticated != true)
                throw new BusinessLogicException("User unauthorized.");
            return httpContextAccessor.HttpContext.User.Claims;
        }

        public Guid GetUserId()
        {
            var claims = GetClaims();
            var userClaim = claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.NameId);
            if (userClaim == null)
                throw new BusinessLogicException("User id not found in token");

            return Guid.Parse(userClaim.Value);
        }

        public string GetPolicy()
        {
            var claims = GetClaims();
            var policy = claims.FirstOrDefault(x => x.Type == JwtConstants.Policy);
            return policy?.Value;
        }

        public string GetEmail()
        {
            var claims = GetClaims();
            var phoneClaim = claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Sub);
            if (phoneClaim == null)
                throw new BusinessLogicException("User email not found in token");

            return phoneClaim.Value;
        }
    }
}
