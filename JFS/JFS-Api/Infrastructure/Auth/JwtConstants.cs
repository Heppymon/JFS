﻿namespace JFS_Api.Infrastructure.Auth
{
    public class JwtConstants
    {
        public const string DeviceId = "Device_Id";
        public const string Policy = "Policy_use";
        public const string Issuer = "sso.ru";
        public const string Audience = "*.ru";
        public const string Role = "Role";
        public const string SymmetricSecurityKeyString = "123";
        public const string VerificationSent = "VerificationSent";
        public const string EmailConfirmed = "EmailConfirmed";
    }
}
