﻿using JFS_Api.Infrastructure.Auth;
using JFS_Api.MIddleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using JwtConstants = JFS_Api.Infrastructure.Auth.JwtConstants;

namespace JFS_Api.Infrastructure
{
    public static class AuthConfiguration
    {
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtConstants.SymmetricSecurityKeyString)), // TODO: Read on config
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        NameClaimType = JwtRegisteredClaimNames.Sub,
                        ClockSkew = TimeSpan.FromMinutes(5),
                        RoleClaimType = JwtConstants.Role
                    };
                    x.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            if (context.Exception.GetType() 
                            == typeof(Microsoft.IdentityModel.Tokens.SecurityTokenExpiredException))
                            {
                                context.Response.Headers.Add("Token-Expired", "true");
                            }

                            return Task.CompletedTask;
                        }
                    };
                });
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap.Clear();

            services.AddHttpContextAccessor();
            services.AddScoped<IJwtSecurity, JwtSecurity>();
            services.AddSingleton<IAuthorizationMiddlewareResultHandler, OverrideErrorMiddlewareResultHandler>();

            services.AddAuthorization(options =>
            {
                var policies = new[]
                {
                    JwtConstants.VerificationSent,
                    JwtConstants.EmailConfirmed
                };

                foreach (var p in policies)
                    options.AddPolicy(p, policy => policy.RequireClaim(JwtConstants.Policy, p));
            });

            return services;
        }
    }
}
