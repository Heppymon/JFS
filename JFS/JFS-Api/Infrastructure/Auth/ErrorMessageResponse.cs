﻿namespace JFS_Api.Infrastructure.Auth
{
    public class ErrorMessageResponse
    {
        public string Message { get; set; }
        public string MessageId { get; set; }
    }
}
