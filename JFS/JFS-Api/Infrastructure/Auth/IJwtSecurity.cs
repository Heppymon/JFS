﻿namespace JFS_Api.Infrastructure.Auth
{
    public interface IJwtSecurity
    {
        string JwtGenerate(Guid userId, string email, string policyLevel);
        Guid GetUserId();
        string GetEmail();
        string GetPolicy();
    }
}
