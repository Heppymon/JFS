﻿using JFS_Api.Extension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;
using System.Net;

namespace JFS_Api.MIddleware
{
    public class OverrideErrorMiddlewareResultHandler : IAuthorizationMiddlewareResultHandler
    {
        private readonly AuthorizationMiddlewareResultHandler defaultHandler = new AuthorizationMiddlewareResultHandler();

        public async Task HandleAsync(RequestDelegate requestDelegate, HttpContext httpContext, AuthorizationPolicy authorizationPolicy, PolicyAuthorizationResult policyAuthorizationResult)
        {
            await defaultHandler.HandleAsync(requestDelegate, httpContext, authorizationPolicy, policyAuthorizationResult);

            if (httpContext.Response.StatusCode == (int)HttpStatusCode.Forbidden)
                await httpContext.SendResponse("No access", "Forbidden", HttpStatusCode.Forbidden, null);

            if (httpContext.Response.StatusCode == (int)HttpStatusCode.Unauthorized)
                await httpContext.SendResponse("User is Unauthorized", "Unauthorized", HttpStatusCode.Unauthorized, null);
        }
    }
}
