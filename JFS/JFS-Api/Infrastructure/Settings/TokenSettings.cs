﻿namespace JFS_Api.Infrastructure.Settings
{
    public class TokenSettings
    {
        public int RefreshTokenExpirationDays { get; set; }
        public int AutoRefreshTokenDays { get; set; }
        public int AccessTokenLifeMinutes { get; set; }
    }
}
