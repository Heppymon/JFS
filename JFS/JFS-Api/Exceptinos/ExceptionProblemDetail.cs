﻿using Microsoft.AspNetCore.Mvc;

namespace JFS_Api.Exceptinos
{
    public class ExceptionProblemDetail : ProblemDetails
    {
        public ExceptionProblemDetail(Exception exception)
        {
            Title = exception.Message;
            Status = StatusCodes.Status400BadRequest;
            Detail = exception.StackTrace;
        }
    }
}
