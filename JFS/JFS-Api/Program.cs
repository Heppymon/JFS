using Hellang.Middleware.ProblemDetails;
using MediatR;
using JFS_Api.Infrastructure;
using JFS_Api.Exceptinos;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddMvc();
builder.Services.AddJwtAuthentication();
builder.Services.AddMediatR(typeof(BusinessLogicException).Assembly/*, typeof(DbDevice).Assembly*/);
builder.Services.AddProblemDetails(options =>
{
    options.IncludeExceptionDetails = (context, ex) =>
    {
        var environment = context.RequestServices.GetRequiredService<IWebHostEnvironment>();
        return environment.IsDevelopment();
    };

    options.Map<Exception>(exception => new ExceptionProblemDetail(exception));

    options.OnBeforeWriteDetails = (ctx, pr) =>
    {
        //logging
        //logger.LogError("Exception Occurred!!!!");
        //logger.LogError(pr.Detail);
        //logger.LogError(pr.Instance);
    };
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
    app.UseProblemDetails();
else
    app.UseMiddleware<ExeptionMiddleware>();

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.UseAuthorization();
app.UseAuthentication();
app.MapControllers();

app.Run();
